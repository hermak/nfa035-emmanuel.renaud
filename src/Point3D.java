
public class Point3D {
	private int coorX;
	private int coorY;
	private int coorZ;

	public Point3D(int x, int y, int z){
		this.coorX = x;
		this.coorY = y;
		this.coorZ = z;
		System.out.println("ok, objet cr��");
	}

	public Point3D(){
		this(0, 0, 0);
	}
	
	public int getCoorX() {
		return this.coorX;
	}
	public void setCoorX(int coorX) {
		this.coorX = coorX;
	}
	public void transCoorX(int dx) {
		this.coorX += dx;
	}
	
	public int getCoorY() {
		return coorY;
	}
	
	/**
	 * 
	 * @param coorY
	 */
	public void setCoorY(int coorY) {
		this.coorY = coorY;
	}
	
	/**
	 * R�sum� du r�le de la m�thode.
	 * 
	 * @param coorY Coordonn�es Y
	 * @since 1.0
	 * 
	 * 
	 */
	public void transCoorY(int dy) {
		this.coorY += dy;
	}
	
	public int getCoorZ() {
		return coorZ;
	}
	public void setCoorZ(int coorZ) {
		this.coorZ = coorZ;
	}
	public void transCoorZ(int dz) {
		this.coorZ += dz;
	}
	
	public String toString() {
	    return "point situ� au coordonn�es x="+this.coorX+", y="+this.coorY+" et z="+this.coorY;
	}

} 
